// VARIABEL

// 1. Konstanta
// const kelasA = 1;
// kelasA = 10;
// console.log(kelasA);

// // 2. Variabel
// var kelasB = 7;
// kelasB = 100;
// console.log(kelasB);

// 3. Let

// TIPE DATA
var nama = "amin"; //string
var umur = 20; //number
var nikah = false; //boolean
var dataKosong = null; //null
var notFound = undefined; //undefined
var dataDiri = { nama: "amin", umur: 20, nikah:false};
console.log(dataDiri);


